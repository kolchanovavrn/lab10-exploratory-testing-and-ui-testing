package org.example;


import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;

import org.openqa.selenium.chrome.ChromeDriver;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import org.testng.annotations.Test;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class TestBooking {

    /**
     * Test case on finding the hotel by name on specific dates
     */

    @Test
    public void testBookingSupportPage() {

        System.setProperty("webdriver.chrome.driver", "/Users/liza/Downloads/chromedriver");
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.onetwotrip.com/");

        System.out.println("Page Title is " + driver.getTitle());

        Assert.assertEquals("Авиабилеты дешево, купить билеты на самолет онлайн, поиск лучшей цены на OneTwoTrip", driver.getTitle());

        WebElement helpButton = driver.findElement(By.xpath("/html/body/div/div/div/header/div/div[2]/div[1]/div[4]/a"));

        helpButton.click();

        System.out.println("Page Title is " + driver.getTitle());

        Assert.assertEquals("Поддержка OneTwoTrip", driver.getTitle());

        driver.quit();

    }


    @Test
    public void testBookingLanguageChange() {

        System.setProperty("webdriver.chrome.driver", "/Users/liza/Downloads/chromedriver");
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.onetwotrip.com/");

        System.out.println("Page Title is " + driver.getTitle());

        Assert.assertEquals("Авиабилеты дешево, купить билеты на самолет онлайн, поиск лучшей цены на OneTwoTrip", driver.getTitle());

        WebElement languageChange = driver.findElement(By.xpath("/html/body/div/div/div/header/div/div[2]/div[3]"));

        languageChange.click();

        WebElement ukrainianLanguageButton = driver.findElement(By.xpath("/html/body/div/div/div/header/div/div[2]/div[3]/div[1]/a[2]/div"));

        ukrainianLanguageButton.click();

        System.out.println("Page Title is " + driver.getTitle());

        Assert.assertEquals("Купити дешеві авіаквитки онлайн на OneTwoTrip.com. Купити квитки на літак дешево.", driver.getTitle());

        driver.quit();
    }

}